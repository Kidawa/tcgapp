#!/bin/sh

# cleaning old dataset
jq '.cards|map(del(.id)|select(.name))' cards.json > clean.json

# auto-generate ids and structure
jq '{"cards": map(select(.name))|to_entries|map({"id": (1+.key)} + .value)}' clean.json > final.json


